﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20240408 김진재
// 어소트락 게임아카데미 C# 강의 17화
// https://youtu.be/_YWqlODLugI?si

// Eunmeration, 열거한다는 의미이다.

// 오브젝트, 클래스 등의 상태나 종류 등을 편하게 정돈할 수 있는 키워드
// 예를 들어 아이템의 경우 포션, 장비, 재료 등의 분류가 있다고 하면
// 이 분류들을 이용해 아이템의 종류를 체계적으로 나눌수 있다.

// 사용자 정의 자료형인 class, struct와 함께 해당된다.
// struct와 함께 값형에 해당된다.
// 솔직히 쓰는 법을 잘 모르겠다.

namespace Chapter_04
{
    internal class cs4_09_Enum // 열거형, 열거자
    {
        public enum Item
        {
            Coffee = 1, // 디폴트는 0이지만 아래는 1부터 시작이라 1 대입함
            Tea,
            Coke,
            Fanta
        }

        static void Main9(string[] args)
        {
            string result = Order(Item.Coke, 5); // Coke, 5잔
            Console.WriteLine(result);
        }

        static string Order(Item item, int qty/*갯수*/)
        {
            switch (item)
            {
                case Item.Coffee:
                    return $"주문하신 음료는 {Item.Coffee} 이며 {qty}개 입니다."; // $을 "" 시작전에 붙이면 안에 있는 변수 같은 것들을 사용할 수 있다.
                case Item.Tea:
                    return $"주문하신 음료는 {Item.Tea} 이며 {qty}개 입니다.";
                case Item.Coke:
                    return $"주문하신 음료는 {Item.Coke} 이며 {qty}개 입니다.";
                case Item.Fanta:
                    return $"주문하신 음료는 {Item.Fanta} 이며 {qty}개 입니다.";
                default:
                    return "Invalid item selected";
            }
        }
    }
}