﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20240330 김진재
// 어소트락 게임아카데미 C# 강의 5화
// https://youtu.be/sRddPJlbvj0?si=bsQakVJX4UT3RBUv

// 코드가 실행되는 순서는 한 줄 한 줄씩이다.
// 위에서 부터 아래로 한 줄씩 OS가 내 프로그램을 읽고 실행시켜주는 것


namespace Chapter_04
{
    internal class cs4_04_CodeView
    {
        static void Main4(string[] args)
        {
            Console.WriteLine("디버깅을 하기위해선 F10을 누르자");
            Console.WriteLine("F10을 누르면 해당 줄까지 코드가 진행이 된다.");
            Console.WriteLine("이것으로 코드의 진행상황과 문제를 알 수 있다.");
        }
    }
}
