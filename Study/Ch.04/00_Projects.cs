﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20240327 김진재
// 어소트락 게임아카데미 C# 강의 2화
// https://youtu.be/veAAkwHQnfg?si=ngeuNSKk9G9wAIx7
// 다중 프로젝트 & using 개념설명

// 솔루션
// 게임을 만들 경우 게임을 만들기 위해 필요한 여러 요소가 필요하다.
// 주로 그러한 요소들은 프로젝트(여러 스크립트) 별로 진행되는데
// 그러한 요소들을 한번에 관리할 수 있게 도와주는 것이
// Visual Studio에서 쓰이는 솔루션이다.
// 솔루션 > 프로젝트 > 클래스(스크립트)

// F5는 빌드를 의미한다.
// 프로젝트 하나마다 exe를 만든다.
// 솔루션 우클릭 속성에 들어가면 선택한 프로젝트가 자동으로
// 빌드의 대상으로 인식 되게 할 수 있다.

// 솔루션 내에서 폴더를 만들어도 실제로 폴더가 생기지 않아서
// 분류하는데 도움을 줄 수 있다.

namespace Chapter_04
{
    internal class cs4_00_Projects
    {
    }
}
