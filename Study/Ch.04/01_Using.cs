﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20240327 김진재
// 어소트락 게임아카데미 C# 강의 2화
// https://youtu.be/veAAkwHQnfg?si=ngeuNSKk9G9wAIx7
// 다중 프로젝트 & using 개념설명

// Using이란
// using System;
// 기존에 작성해놓은 코드들을 System이라는 별도의 카테고리로 
// 분류하여 끌어다 쓰겠다는 용도로 쓰인다.
// 여기서 System은 namespace에 속한다.

namespace Chapter_04
{
    internal class cs4_01_Using
    {
        static void Main1(string[] args)
        {
            Console.WriteLine("Console을 클릭하고 F12를 누르면 System으로 갈 수 있다.");
            // Console은 Class에 속한다.
            // using을 통해 System이라는 namespace를 가진 Console이라는 Class에서 참조할 수 있다.
        }
    }
}
