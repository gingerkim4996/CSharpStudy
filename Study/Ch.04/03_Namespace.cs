﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20240327 김진재
// 어소트락 게임아카데미 C# 강의 4화
// https://youtu.be/EhR0g5gBGaA?si=hFFRLcEc6aU3eA-q

// namespace란
// 코드들을 어떠한 형태로 구분하는 용도로 쓰임.
// 예를 들어 아래의 경우 Chapter4라는 의미에서 구분되고 있음.
// 굳이 수식하지 않아도 되는 경우 굳이 없어도 된다.
// 위에 using system의 경우 system이 네임스페이스에 해당한다.

namespace Chapter_04
{
    internal class cs4_03_Namespace
    {
        static void Main3(string[] args)
        {
            Console.WriteLine("Console을 클릭하고 F12를 누르면 System으로 갈 수 있다.");
            // Console은 Class에 속한다.
            // using을 통해 System이라는 namespace를 가진 Console이라는 Class에서 참조할 수 있다.
        }
    }

    namespace HP  // 코드들을 어떠한 형태로 구분하는 용도로 쓰임.
    {             // 예를 들어 이 경우 HP가 앞에 붙은만큼 HP 관련된 코드들을 담는다고 봐야함
                  // 아래와 같이 Potion 등 이름이 같은 클래스들이 있을 경우 구분에 용이하다.
        class Potion // 앞에 네임스페이스가 원래 붙으나 생력된 것으로 본래는 HP.Potion
        {
            // HP가 차는 포션
        }
    }

    namespace MP  // 코드들을 어떠한 형태로 구분하는 용도로 쓰임.
    {             // 예를 들어 이 경우 MP가 앞에 붙은만큼 MP 관련된 코드들을 담는다고 봐야함
                  // 아래와 같이 Potion 등 이름이 같은 클래스들이 있을 경우 구분에 용이하다.
        class Potion // 앞에 네임스페이스가 원래 붙으나 생력된 것으로 본래는 MP.Potion
        {
            // MP가 차는 포션
        }
    }
}

