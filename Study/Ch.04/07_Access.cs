﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20240404 김진재
// 어소트락 게임아카데미 C# 강의 7화
// https://youtu.be/-JD5ry7P1iU?si=dYKuD2tdn9uLH40X

// 1. 접근제한자
// 객체지향의 캡슐화 은닉화를 대표하는 문법
// 접근제한 지정자
// 클래스, 변수, 함수, enum 앞에 적게 된다.

// 2. Public
// 클래스의 외부에서 접근이 가능하다.

// 3. Private
// 클래스의 내부에서만 접근이 가능하다.
// [SeiralizedField]를 사용하면 접근이 가능하다.

// 4. Protected
// 클래스의 내부에서만 접근이 가능하며
// 상속시 자식이 사용이 가능하다.
// 또한 외부에서 호출시 보호수준이 높아 접근할 수 없다고 뜬다.


namespace Chapter_04
{

    internal class cs4_07_Access // 접근제한자
    {
        public class Example1
        {
            private void Example2()
            {
            }
            protected int Example3 = 1;
        }

        public class Player
        {   // 기본값은 Private이며
            // 이는 외부의 접근을 허용하지 않는 상태이다.
            public int hp;    // 공개
            private int mp;    // 같은 클래스 내에서만 공개
            protected int atk;  // 같은 클래스 내에서만 사용가능하지만
                                // 자식(상속된 것)에게도 공개

            public void Fight() // 함수도 접근자가 사용이 가능하다. 마찬가지로 private 사용시 접근이 불가능
            {
                mp = 50;          // 같은 클래스 내에 있는 Fight 함수 내에서도 private인 mp를 호출이 가능
                atk = 10;
                Console.WriteLine("Fight!");
                Console.WriteLine(hp);
                Console.WriteLine(mp);
                Console.WriteLine(atk); // 외부에서 호출시 보호수준이 높아 접근할 수 없다고 뜬다.
            }
        }

        class Program
        {
            static void Main7(string[] args)
            {
                Player pl = new Player();
                // 만들어진 객체의 내용을 사용하기 위해
                // 객체의 이름.을 사용한다.

                pl.hp = 51;
                pl.Fight(); // public 사용으로 외부에서 접근이 가능해졌음
            }
        }
    }
}
