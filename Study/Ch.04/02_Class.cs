﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20240327 김진재
// 어소트락 게임아카데미 C# 강의 3화
// https://youtu.be/ivYyzsHujqY?si=aM1GH55fJz_vcPff

// Class란
// 객체지향형 프로그래밍 언어에서 가장 중요한 개념으로
// 내가 설계한 프로그램이 만들어지기 위한 설계도에 가까운 개념이다.
// 또는 붕어빵 틀이라는 비유를 쓰기도 한다.
// 자신이 구상한 요소들을 찍어내는 용도로 쓰인다.

// 20240409 김진재
// 함수 내에서는 클래스를 선언할 수 없다.
// 클래스는 생성자를 가질수 있다.
// 클래스는 참조형이다.
// 클래스는 enum, structure와 같이 사용자 정의 자료형이다.
// 사용자 정의 자료형들은 매개변수가 없다.
// public과 private과 protected 등의 접근 제어 방식이 존재한다.

// 생성자의 매개변수, this, 인스턴스의 매개변수

// 클래스와 생성자와 인스턴스의 관계
// Case 1. 생성자 안에 있는 변수를 출력하는 법
// 변수초기화 o 매개변수 o this o
// 변수초기화 o 매개변수 x this x
// 변수초기화 x 매개변수 x this x
// 변수초기화 o 매개변수 x this o

// Case 2. 초기화 된 멤버 변수를 출력하는 법
// 변수초기화 o 매개변수 o this x
// 변수초기화 o 매개변수 x this x

// Case 3. main 메서드의 변수를 출력하는 법
// 변수초기화 x 매개변수 o this o
// 변수초기화 x 매개변수 x this x

namespace Chapter_04
{
    internal class cs4_02_Class
    {
        public class cl // (변수 초기화 o, 매개변수 o, this o) 생성자 안에 있는 변수 출력하는 법1 
        {
            public string a = "매개변수를 사용했고 제일 처음 초기화"; // b를 10으로 초기화

            public cl(string a) // 매개변수를 사용한 생성자
            {
                this.a = "매개변수를 사용한 this"; // 생성자의 매개변수로 받은 값을 사용하여 필드를 초기화
            }
        }
        public class cll // (변수 초기화 o, 매개변수 o, this x) 멤버 변수를 출력하는 법1 
        {
            public string b = "매개변수를 사용했고 제일 처음 초기화"; // b를 10으로 초기화

            public cll(string b) // 매개변수를 사용한 생성자
            {
                b = "매개변수를 사용한 this"; // 생성자의 매개변수로 받은 값을 사용하여 필드를 초기화
            }
        }
        public class clll // (변수 초기화 o, 매개변수 x, this x) 생성자 안에 있는 변수 출력하는 법2 
        {
            public string c = "매개변수를 사용안했고 제일 처음 초기화"; // b를 10으로 초기화

            public clll() // 매개변수를 사용하지 않은 생성자
            {
                c = "매개변수와 this 사용 안함"; // 생성자의 매개변수로 받은 값을 사용하여 필드를 초기화
            }
        }
        public class cllll // (변수 초기화 x, 매개변수 x, this x) 생성자 안에 있는 변수 출력하는 법3 
        {
            public string d; //

            public cllll() // 매개변수를 사용하지 않은 생성자
            {
                d = "매개변수를 사용하지 않았고 처음 초기화도 안한 것"; // 생성자의 매개변수로 받은 값을 사용하여 필드를 초기화
            }
        }
        public class clllll // (변수 초기화 o, 매개변수 x, this o,) 생성자 안에 있는 변수 출력하는 법4 
        {// 클래스는 매개변수가 없다.
            public string e = "매개변수를 사용안했고 제일 처음 초기화"; // a를 20으로 초기화
            
            public clllll() // 매개변수를 쓰지 않은 생성자
            {
                this.e = "매개변수를 사용안한 this"; // 생성자의 매개변수로 받은 값을 사용하여 필드를 초기화
            }
        }
        public class cllllll // (변수 초기화 o, 매개변수 x, this x) 멤버 변수를 출력하는 법2 
        {
            public string f = "제일 처음 초기화 하고 아무것도 안한 것"; // b를 10으로 초기화

            public cllllll() // 매개변수를 사용하지 않은 생성자
            {
                f = "엥 이건머임"; // 생성자의 매개변수로 받은 값을 사용하여 필드를 초기화
            }
        }
        public class clllllll // (변수 초기화 x, 매개변수 o, this o) main 메서드에서 출력하는 법1 
        {
            public string g; //

            public clllllll(string gg) // 매개변수를 사용한 생성자
            {
                this.g = "??"; // 생성자의 매개변수로 받은 값을 사용하여 필드를 초기화
            }
        }
        public class cllllllll // (변수 초기화 x, 매개변수 o, this x) main 메서드에서 출력하는 법2 
        {
            public string h; //

            public cllllllll(string gg) // 매개변수를 사용한 생성자
            {
                h = "엥"; // 생성자의 매개변수로 받은 값을 사용하여 필드를 초기화
            }
        }
        class Program
        {
            static void Main(string[] args)
            {
                cl CL = new cl("매개변수 있음");
                // 위에 매개변수가 있으면 생성자 호출시 인수를 전달함

                cll CLL = new cll("매개변수 있음");
                // 위에 매개변수가 있으면 생성자 호출시 인수를 전달함

                clll CLLL = new clll();
                // 위에 매개변수가 없으면 인수 없어도 됨

                cllll CLLLL = new cllll();
                // 위에 매개변수가 없으면 인수 없어도 됨

                clllll CLLLLL = new clllll();
                // 위에 매개변수가 없으면 인수 없어도 됨

                cllllll CLLLLLL = new cllllll();
                // 위에 매개변수가 없으면 인수 없어도 됨

                clllllll CLLLLLLL = new clllllll("매개변수 있음");
                // 위에 매개변수가 있으면 생성자 호출시 인수를 전달함

                cllllllll CLLLLLLLL = new cllllllll("매개변수 있음");
                // 위에 매개변수가 있으면 생성자 호출시 인수를 전달함

                Console.WriteLine(CL.a);
                Console.WriteLine(CLL.b);
                Console.WriteLine(CLLL.c);
                Console.WriteLine(CLLLL.d);
                Console.WriteLine(CLLLLL.e);
                Console.WriteLine(CLLLLLL.f);
                Console.WriteLine(CLLLLLLL.g);
                Console.WriteLine(CLLLLLLLL.h);


                Console.WriteLine("Class란");
                Console.WriteLine("객체지향형 프로그래밍 언어에서 가장 중요한 개념으로");
                Console.WriteLine("내가 설계한 프로그램이 만들어지기 위한 설계도에 가까운 개념이다.");
                Console.WriteLine("또는 붕어빵 틀이라는 비유를 쓰기도 한다.");
                Console.WriteLine("자신이 구상한 요소들을 찍어내는 용도로 쓰인다.");
            }
        }
    }
}
