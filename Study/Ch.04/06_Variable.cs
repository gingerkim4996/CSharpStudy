﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

// 20240330 김진재
// 어소트락 게임아카데미 C# 강의 7화
// https://youtu.be/-JD5ry7P1iU?si=dYKuD2tdn9uLH40X
// 티스토리 블로그 eqsrainy의 8.멤버변수와 지역변수
// https://eqsrainy.tistory.com/16

// 1. 멤버 변수
// 클래스의 멤버는 2가지로 나뉘는데 '멤버 변수(필드)'와 '멤버 메서드'가 있다.
// 이 중 멤버 변수는 2가지로 나뉘는데 '클래스 변수'와 '인스턴스 변수'이다.
// 클래스 변수는 static을 사용하기 때문에 스태틱 변수라고도 불린다.

// 요약 -> '멤버 변수'란 클래스 내에서 선언되는 변수를 뜻하며
// 이들은 '클래스 변수'와 '인스턴스 변수'로 구성되어 있다.

// 1.1 클래스 변수
// 클래스 변수는 static으로 선언하기 때문에
// 클래스명 변수명 = new 클래스명으로 접근하지 않아도 사용할 수 있으며
// new를 사용할 경우 static은 제외하고 생성된다.

// 1.2 인스턴스 변수
// 선언 자체는 평범하게 public으로 선언할 수 있다.
// 다만 클래스명 변수명 = new 클래스명으로 접근해야 한다.
// 때문에 클래스 변수의 경우 본래의 클래스명으로 접근이 가능하지만
// 인스턴스 변수의 경우 클래스명에 새로운 변수명을 붙여 호출이 가능하다.

// 2. 지역 변수
// 지역 변수는 선언된 해당 함수/메서드 내에서만 유효하며
// 다른 외부에서 접근이 불가능하다.
// 다만 아래(밑의 3번 예시)와 같이 함수를 호출하여 반환된 결과를 통해
// 그 값을 다른 클래스에서 이용이 가능하다.

// ------------- 클래스 -------------
// --- 멤버변수 --- --- 멤버메서드 ---
// ---클래스 변수--
//   (ㄴ스태틱 변수)
// --인스턴스 변수-

namespace Chapter_04
{
    internal class cs4_06_Variable // 변수
    {
        class pcSeller
        {
            // 1. 클래스 변수(스태틱 변수)
            // name은 이 클래스의 클래스 변수로 static 키워드로 선언되어
            // 다른 클래스에서도 사용이 가능하다.
            // 변수로서 접근하고자 할 때 이 클래스의 이름인
            // pcSeller와 이 변수의 이름인 name을 합쳐
            // pcSeller.name으로 호출할 수 있다.
            static public string name = "늑대와여우컴퓨터";

            // 2. 인스턴스 변수
            // age는 이 클래스의 인스턴스 변수로 선언 자체는 public이 붙을뿐 간단하다
            // public을 통해 다른 클래스에서도 사용이 가능하다.
            // 다만 사용하고자 하는 다른 클래스에서
            // 클래스명 변수명 = new 클래스명으로 클래스를 변수로 선언하여
            // 접근이 가능하게 만들어야 한다.
            // 이 경우엔 pcSeller com = new pcSeller();로 작동이 가능하다.
            public int age = 10; // << 멤버 변수


        }
        // 3. 지역 변수
        // 지역 변수는 선언된 해당 함수/메서드 내에서만 유효하며
        // 다른 외부에서 접근이 불가능하다.
        // 다만 아래와 같이 함수를 호출하여 반환된 결과를 통해
        // 그 값을 다른 클래스에서 이용이 가능하다.
        static int A(int a, int b)
        {
            int result = a + b;
            return result;
        }

        static void Main6(string[] args) // static void이기에 클래스 변수에 해당된다.
        {
            // 1. 클래스 변수(스태틱 변수)
            // name은 이 클래스의 클래스 변수로 static 키워드로 선언되어
            // 다른 클래스에서도 사용이 가능하다.
            // 변수로서 접근하고자 할 때 이 클래스의 이름인
            // pcSeller와 이 변수의 이름인 name을 합쳐
            // pcSeller.name으로 호출할 수 있다.
            Console.WriteLine(pcSeller.name);

            // 2. 인스턴스 변수
            // age는 이 클래스의 인스턴스 변수로 선언 자체는 public이 붙을뿐 간단하다
            // public을 통해 다른 클래스에서도 사용이 가능하다.
            // 다만 사용하고자 하는 다른 클래스에서
            // 클래스명 변수명 = new 클래스명으로 클래스를 변수로 선언하여
            // 접근이 가능하게 만들어야 한다.
            // 이 경우엔 pcSeller com = new pcSeller();로 작동이 가능하다.
            pcSeller com = new pcSeller();
            Console.WriteLine(com.age);

            // 3. 지역 변수
            // 지역 변수는 선언된 해당 함수/메서드 내에서만 유효하며
            // 다른 외부에서 접근이 불가능하다.
            // 다만 아래와 같이 함수를 호출하여 반환된 결과를 통해
            // 그 값을 다른 클래스에서 이용이 가능하다.
            // 값형의 처리라고도 한다.
            // return 값을 받아오기도 한다.
            int resul = A(1, 2);
            Console.WriteLine(resul);
        }
    }
}
