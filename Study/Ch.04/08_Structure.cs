﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20240408 김진재
// csharpstudy - [C# 기초] C# 구조체 (struct)
// https://youtu.be/lLAzAQZfNXA

// 1. 값 형식의 특성:
// 구조체는 값 형식(Value Type)이므로
// 스택에 할당되어 메모리 관리가 간단하다.

// 작은 크기의 데이터를 다룰 때 효율적이다.
// 큰 크기의 데이터를 다룰 때 쓰려면
// 값이 복제돼서 메모리를 많이 차지하게 되기 때문

// 값 형식으로서의 구조체 특성은
// 복사가 발생할 때 값이 복제되어 기존의 값을 잃어버리거나
// 값이 변경되는 등의 의도치 않은 부작용을 방지할 수 있다.

// 2. 불필요한 기능 제한
// 구조체는 클래스와 달리 상속이나 다형성을 지원하지 않는다.
// 따라서 간단한 데이터 구조를 표현할 때
// 클래스보다 불필요한 기능을 제한하기에
// 코드의 간결성을 유지할 수 있다.

// 3. 데이터의 의미 전달
// 구조체는 주로 데이터의 의미를 전달하는 데 사용된다.
// 예를 들어, 날짜, 시간, 벡터 등의 간단한 데이터 구조를
// 표현할 때 구조체가 적합하다.

// 다만 클래스는 주로 객체의 행동과 상태를 모두 포함하는
// 더 복잡한 객체를 표현하는 데 사용된다.

// 4. 불변성과 안전성
// 구조체를 사용하면 불변성(Immutability)을 유지하기 쉽다.

// 값 형식으로 취급되는 구조체는 값이 변경되지 않는 한
// 항상 같은 값을 가지므로 불변성을 유지하기 쉽기 때문이다.
// 또한 구조체는 값이 복제되어 데이터의 안전성을 보장하기 때문에
// 스레드 안전성(Thread Safety)을 유지하기 쉽다.

// 5. 성능 향상:
// 작은 크기의 데이터를 다룰 때 구조체를 사용하면
// 메모리 사용량이 감소하여 성능이 향상될 수 있다.
// 특히 작은 크기의 데이터를 한번에 여럿 다룰 때
// 더 효율적이다.

namespace Chapter_04
{
    internal class cs4_08_Structure // 구조체
    {
        struct Passport
        {
            public string Name;
            public int Age;
            public string Destination;
            public int Date;
            public Passport(string name, int age, string destination, int date)
            {
                Name = name;
                Age = age;
                Destination = destination;
                Date = date;
            }
        }

        static void Main8(string[] args)
        {
            #region ConsoleLine
            Console.WriteLine("이름을 적으세요.");
            string x = (Console.ReadLine());
            Console.WriteLine("나이를 적으세요.");
            int y = int.Parse(Console.ReadLine());
            Console.WriteLine("목적지를 적으세요.");
            string z = (Console.ReadLine());
            Console.WriteLine("생일을 적으세요.");
            int w = int.Parse(Console.ReadLine()); 
            #endregion

            Passport pspt = new Passport(x,y,z,w);
            //pspt.Name = x;
            //pspt.Age = y;
            //pspt.Destination = z;
            //pspt.Date = w;

            Draw(pspt);
        }

        static void Draw(Passport pspt) // 하나의 파라미터만 주고 받기에 편하다.
        {
            Console.WriteLine("이름 : " + pspt.Name);
            Console.WriteLine("나이 : " + pspt.Age);
            Console.WriteLine("목적지 : " + pspt.Destination);
            Console.WriteLine("생일 : " + pspt.Date);
        }
    }
}
