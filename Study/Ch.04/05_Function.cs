﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20240330 김진재
// 어소트락 게임아카데미 C# 강의 7화
// https://youtu.be/-JD5ry7P1iU?si=dYKuD2tdn9uLH40X

// 함수란?
// 코드가 작동할 때 내외부에서 불러와 값을 변동하거나 계산할 수 있는 것으로
// 예를 들어 게임의 "플레이어"라는 클래스가 있을 경우
// Move, Attack, Skill 등을 구현하기 위해선 함수가 사용된다.

// 게임을 만들 경우 필요한 요소들
// 게임을 만든다 -> 프로젝트
// 주인공을 구성한다 -> 클래스
// 주인공의 공격력이 있다 -> 멤버 변수
// 주인공이 공격한다 -> 멤버 함수

namespace Chapter_04
{
    // 게임 시스템 클래스
    internal class cs4_05_Function
    {
        // 플레이어 클래스
        class Player
        {
            // 플레이어의 체력과 공격력 변수
            public int hp = 30; // 멤버 변수
            public int atk = 5; // 멤버 변수
            // 멤버 변수란 클래스 내부에 있어서 내부 변수임.

            // 플레이어의 일반 공격 메서드
            public void Attack(Monster mob)
            {
                mob.hp -= atk;
                Console.WriteLine("Monter가 공격에 맞았습니다! ");
            }

            // 플레이어의 급소 공격 메서드
            public void BigAttack(Monster mob)
            {
                mob.hp -= atk * 5;
                Console.WriteLine("Monter가 급소에 맞았습니다! ");
            }

            // 플레이어의 공격 시스템
            public void PlAtkSystem(Monster mob)
            {
                Random rnd = new Random();
                int randomNumber = rnd.Next(1, 11); // 1부터 10까지의 랜덤 값 생성
                for (int i = 1; i <= 10; i++)
                {
                    if (i == randomNumber) // 그냥 공격 성공
                    {
                        Console.WriteLine("\n공격에 성공했습니다! " + "-" + atk);
                        Attack(mob);
                    }
                    else if (i == randomNumber && i % 3 == 0) // 3의 배수인 경우 크리티컬
                    {
                        Console.WriteLine("\n급소를 노렸습니다! " + "-" + atk * 5);
                        BigAttack(mob);
                    }
                }
            }
        }

        // 몬스터 클래스
        class Monster
        {
            // 몬스터의 체력과 공격력 변수
            public int hp = 40; // 멤버 변수
            public int atk = 3; // 멤버 변수

            // 몬스터의 일반 공격 함수
            public void Attack(Player pl)
            {
                pl.hp -= atk;
                Console.WriteLine("Player가 공격에 맞았습니다! ");
            }

            // 몬스터의 급소 공격 함수
            public void BigAttack(Player pl)
            {
                pl.hp -= atk * 2;
                Console.WriteLine("Player가 급소에 맞았습니다! ");
            }

            // 몬스터의 공격 시스템
            public void MobAtkSystem(Player pl)
            {
                Random rnd = new Random();
                int randomNumber = rnd.Next(1, 11); // 1부터 10까지의 랜덤 값 생성
                for (int i = 1; i <= 10; i++)
                {
                    if (i == randomNumber && i % 2 == 0) // 2의 배수인 경우
                    {
                        Console.WriteLine("\n몬스터의 공격입니다! " + "-" + atk);
                        Attack(pl);
                    }
                    else if (i == randomNumber && i % 3 == 0) // 5의 배수인 경우
                    {
                        Console.WriteLine("\n몬스터의 기습입니다! " + "-" + atk * 2);
                        BigAttack(pl);
                    }
                    else if (i == randomNumber)
                    {
                        Console.WriteLine("\n몬스터의 공격을 회피하였습니다! " + "0");
                    }
                }
            }
        }

        // 이들은 게임에서 사용하기 위해 설계된 것으로
        // 위의 클래스들은 설계도라고 볼 수 있다.

        // 게임 시스템 클래스 내부에 게임을 관리하는 클래스
        class GameSystem
        {
            // 메인 메서드: 프로그램 실행 진입점
            // 시작하기 위한 메인 함수
            static void Main5(string[] args)
            {
                // 게임 시작
                StartGame();
            }

            // 게임 시작 메서드
            static void StartGame()
            {

                // 플레이어와 몬스터 객체 생성
                // 아래는 클래스의 설계도대로 몬스터와 클래스를 만드는데
                // 각각 new Monster와 new Player가 된다.
                Monster mob = new Monster();
                Player pl = new Player();

                // 초기 상태 출력
                Console.WriteLine("Player의 체력은 " + pl.hp);
                Console.WriteLine("Monster의 체력은 " + mob.hp);
                Console.WriteLine("공격은 스페이스바 입니다. ");

                // 게임 루프: 플레이어와 몬스터의 체력이 0보다 클 때까지 반복
                while (mob.hp > 0 && pl.hp > 0)
                {
                    // 플레이어 입력 받기
                    ConsoleKeyInfo keyInfo = Console.ReadKey();
                    pl.PlAtkSystem(mob);

                    // 스페이스바 입력 시 몬스터 공격
                    if (keyInfo.Key == ConsoleKey.Spacebar)
                    {
                        mob.MobAtkSystem(pl);
                    }

                    // 현재 상태 출력
                    Console.WriteLine("\n당신의 차례 입니다.");
                    Console.WriteLine("공격은 스페이스바 입니다.");
                    Console.WriteLine("Player의 체력은 " + pl.hp);
                    Console.WriteLine("Monster의 체력은 " + mob.hp);
                }

                // 게임 종료 후 결과 출력
                if (mob.hp <= 0)
                {
                    Console.WriteLine("\n당신의 승리입니다!");
                }
                else if (pl.hp <= 0)
                {
                    Console.WriteLine("\n당신의 패배입니다!");
                }

                // 재시작 여부 확인
                Console.WriteLine("\n다시 시작하려면 스페이스바를 누르세요.\n");
                ConsoleKeyInfo restartKey = Console.ReadKey();
                if (restartKey.Key == ConsoleKey.Spacebar)
                {
                    Console.Clear(); // 콘솔 화면 지우기
                    StartGame();     // 게임 재시작
                }
            }
        }
    }
}