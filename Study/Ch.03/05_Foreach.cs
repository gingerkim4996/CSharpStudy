﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20240305 김진재
// C# 프로그래밍 한빛아카데미 참고
// p171
// foreach 반복문은 컬렉션에 쉽게 반복문을 적용할 때에 사용한다.
// 여기서 컬렉션은 여러 개체가 모여 집합을 이룬 것으로
// 대표적인 컬렉션이 바로 배열이다.

// foreach는 아래와 같은 형태로 사용된다.
// foreach ( 자료형 변수 in 컬렉션(Ex.배열))
// {
// 
// }

// 이 foreach는 대체로 아래와 같은 for문과 같이 쓰인다.
// for (int i = 0; i < 컬렉션.length; i++)
// {
//    자료형 변수 = 컬렉션[i];
// }

namespace Chapter_03
{
    internal class cs3_05_Foreach     // Foreach 반복문
    {
        static void Main5(string[] args)
        {
            // 1. Foreach와 배열
            // 다음 코드는 string 배열을 만들고 foreach 반복문 내부에서
            // 배열의 요소를 차례대로 출력하는 것이다.
            string[] array = { "사과", "배", "포도", "딸기", "바나나" };

            foreach (string item in array)
            {   
                Console.WriteLine(item);
            }
            
            // 2. var 키워드를 사용
            // 자료형을 자동으로 선택하는 var를 사용할 수 있다.
            // foreach에서 배열의 자료형을 var로 받아오는건 가능하지만
            // 처음부터 배열의 자료형을 var로 선언하는 것은 불가능하다.
            string[] array2 = { "사구아", "베", "뽀또", "달기", "반갈죽" }; // var[] array2 X, string[] array2 O

            foreach (var item in array2)
            {
                Console.WriteLine(item);
            }
        }
    }
}
