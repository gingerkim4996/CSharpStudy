﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20240312 김진재
// C# 프로그래밍 한빛아카데미 참고
// p179
// Continue 키워드는 반복문 내부에서 
// 현재 반복을 멈추고 다음 반복을 진행하게 만드는 키워드
// 다만 코드를 복잡하게 만들 위험이 있어 굳이 쓰지 않아도 된다.

namespace Chapter_03
{
    internal class cs3_09_Continue    // Continue 키워드
    {
        static void Main9(string[] args)
        {
            // 1. 홀수만 출력하는 코드
            for (int i = 1; i < 10; i++)
            {
                if(i % 2 == 0) // 짝수를 찾는 반복문
                {
                    continue; // 위에서 짝수일 경우 종결식(i++)로 이동한다.
                }
                Console.WriteLine(i); // 홀수일 경우 i를 출력한다.
            }
            Console.Write('\n');

            // 2. continue를 쓰지 않는 코드1
            for (int i = 1; i <= 10; i++)
            { 
                if(i % 2 == 0)
                {
                    Console.WriteLine(i-1);
                }
            }
            Console.Write('\n');

            // 3. continue를 쓰지 않는 코드2
            for (int i = 1; i < 10; i++)
            {
                if (i % 2 != 0) // 단순히 짝수가 아닌 것만 출력하게 하면 된다.
                {
                    Console.WriteLine(i);
                }
            }
        }
    }
}
