﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20240312 김진재
// C# 프로그래밍 한빛아카데미 참고
// p177
// 
// while (true)
// {
// }
// 위와 같은 반복문은 내부에서 break 키워드를 사용해야 벗어날수 있다.
// 다만 코드를 복잡하게 만들 위험이 있어 굳이 쓰지 않아도 된다.

namespace Chapter_03
{
    internal class cs3_08_Break    // break 키워드
    {
        static void Main8(string[] args)
        {
            // 1. 짝수를 입력받으면 break
            while (true) // while의 조건식이 bool이 답으로 나오는 식이거나 true일 경우 무한 반복한다. 
            {
                Console.WriteLine("숫자를 입력해주세요(짝수 입력시 종료) : ");
                int input = int.Parse(Console.ReadLine()); // input에 int.Parse로 readline의 값을 할당한다.

                if (input % 2 == 1) // 2로 나누었을때 나머지가 1일 경우 홀수
                {
                    Console.WriteLine("홀수입니다. 다시 입력해주세요.");
                }

                if (input % 2 == 0) // 2로 나누었을때 나머지가 1일 경우 짝수
                {
                    Console.WriteLine("짝수입니다. 종료되었습니다.");
                    break;
                }
            }
        }
    }
}
