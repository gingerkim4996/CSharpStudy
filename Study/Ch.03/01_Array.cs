﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20240301 김진재
// C# 프로그래밍 한빛아카데미 참고
// p157
// 배열은 여러 개의 자료를 한꺼번에 다룰 수 있는 자료형이다.
// 배열은 다음과 같은 형식으로 생성한다. 중괄호 내부의 각 자료는 쉼표로 구분한다.
// 자료형[] 이름 = {자료, 자료}
// 본 스크립트 내에서는 본인의 테스트를 위해 한글을 사용한 변수명을 이용하였으나
// 본래 한글을 사용한 변수명 등은 운영체제에서 오류를 발생시킬수 있으니 주의를 요한다.

namespace Chapter_03
{
    internal class cs3_01_Array // 배열
    {
        static void Main1(string[] args)
        {
            // 1. 각종 배열 선언법
            int[] int용배열예시 = { 1, 2, 3, 4, 5 }; // 이해를 돕기 위한 예시로 굳이 한글로 작성함.
            int[] intArray = { 52, 273, 32, 65, 103 };
            long[] longArray = { 52, 273, 32, 65, 103 };
            float[] floatArray = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f };
            double[] doubleArray = { 1.0, 2.0, 3.0, 4.0, 5.0 };
            char[] charArray = { 'a', 'b', 'c', };
            string[] stringArray = { "abc", "가나다", "김진재" };

            // 2. 선언된 배열의 구성원에 대입하기
            intArray[0] = 0;            // intArray의 첫번째 구성(0)은 본래 52이나 0을 대입함
            stringArray[2] = "짱짱맨";  // stringArray의 세번째 구성(2)은 본래 김진재이나 짱짱맨을 대입함

            // 3. 배열의 구성원을 출력하기
            Console.WriteLine(intArray[0]); // 본래 52이던 곳에 0을 대입하여 0으로 출력됨
            Console.WriteLine(intArray[1]); // 273
            Console.WriteLine(stringArray[1]); // abc
            Console.WriteLine(stringArray[2]); // 가나다

            // 4. 배열의 구성원들의 수(배열의 범위) 출력하기
            Console.WriteLine(stringArray.Length); // stringArray의 구성원은 abc와 가나다, 짱짱맨(김진재에서 대체됨)으로 3이 출력됨

            // 5. 배열의 구성원들을 전부 출력하기
            // string.Join을 이용해 출력하기
            string result = string.Join(", ", stringArray); // string.Join을 통해서 StringArray를 result에 string으로 받아옴.
            Console.WriteLine(result);  // 위의 stringArray를 result로 대입하여 string으로 출력.

            // foreach를 이용해 출력하기
            foreach (string str in stringArray)
            {
                Console.WriteLine(str);
            }

            // 6. 배열의 범위 설정하고 출력하기
            int[] int용배열예시2 = new int[100]; // 이해를 돕기 위한 예시로 굳이 한글로 작성함. 
                                                // 배열의 범위를 선언하는 예시로 new int[100]으로 100까지의 수로 보관이 가능함.
            int용배열예시2[2] = 5000;
            int용배열예시2[50] = 123;
            Console.WriteLine(int용배열예시2[0]); // 0에는 아무것도 대입된 것이 없어 0
            Console.WriteLine(int용배열예시2[2]); // 2에는 5000이 대입되어 5000
            Console.WriteLine(int용배열예시2[99]); // 99에는 아무것도 대입된 것이 없어 0
            Console.WriteLine(int용배열예시2[50]); // 50에는 123이 대입되어 123

            foreach (int 예시2 in int용배열예시2)
            {
                Console.Write(예시2);
            }
        }
    }
}
