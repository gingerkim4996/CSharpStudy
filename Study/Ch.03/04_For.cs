﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20240304 김진재
// C# 프로그래밍 한빛아카데미 참고
// p166 ~ p170
// for문은 원하는 횟수만큼 반복하고 싶은 경우에 사용한다.
// for문은 맨 먼저 초기식을 싱행하고 조건식을 확인한다.
// 이때 조건식이 거짓이면 반복문이 끝나고
// 참이면 문장과 종결식을 차례로 실행하고 조건식이 참인지 확인한다.

// 아래 예시는 본문속 1번 1부터 100까지 더하는 반복문

// 1. 초기식을 비교한다. int i = 0;
// 2. 조건식을 비교한다. i <= 100;
// 3. 문장을 실행한다. output += i;
// 4. 종결식을 실행한다. i++;
// 5. 조건식을 비교한다. 이하 반복
// 6. 조건이 거짓이면 반복문을 종료 하고
//    for문 아래에 있는 코드를 실행한다.
//    Console.WriteLine(output);

// 일반적인 for문은 다음과 같은 형태로 특정한 횟수만큼 반복된다.
// for (int i = 0; i < 반복횟수; i++)
// {
//     문장
// }
// 반복 이후 실행될 코드


namespace Chapter_03
{
    internal class cs3_04_For     // For반복문과 역For문
    {
        static void Main4(string[] args)
        {
            // 1. 1부터 100까지 더하는 반복문
            int output = 0;

            for (int i = 0/*초기식*/; i <= 100/*조건식*/; i++/*종결식*/)
            {
                output += i;/*문장*/
            }
            Console.WriteLine(output);


            // 2. 1부터 20까지 곱하는 반복문
            int output2 = 1; // 곱해야하니 디폴트 값은 1이어야한다. 안그러면 0은 곱해도 0이다.

            for(int i = 1/*초기식*/; i <= 20/*조건식*/; i++/*종결식*/)
            {
                output2 *= i;/*문장*/
            }
            Console.WriteLine(output2);


            // 3. a부터 Z까지 출력하는 반복문
            // char는 숫자로도 변환이 가능하기 때문에 i를 증감시켜 다음 문자로 넘어갈 수 있게 된다.
            for (int i = 'a'; i <= 'z'; i++)
            {
                Console.Write((char)i);
            }


            // 4. 역 for문
            // 배열 반복을 뒤에서부터 실행해야 하는 경우 사용
            int[] intArray = { 1, 2, 3, 4, 5, 6 }; // 배열 intArray의 구성원

            for (int i = intArray.Length - 1 /*초기식*/; i >= 0/*중간식*/; i--/*종결식*/)
            {
             // 초기식은 배열 범위(위 배열은 6)에서 1은 뺀 것
             // 중간식은 초기식에 대입된 i의 값인 5
             // 문장에선 5가 출력이 되고
             // 종결식에서 i--가 되어 4가 된다.
             // 중간식의 i는 종결식에서 i--가 된 4가 문장에 대입되어 4가 출력되고 종결식에서 i--되어 3이 된다.
             // 다시 중간식부터 반복 후 i가 0이 되어 계산이 되지 않을 때까지 나오며
             // 0은 계산이 되지 않아 최종으로는 1에서 마무리
                Console.WriteLine(intArray[i]);/*문장*/
            }

            int ii = intArray.Length - 1; // 위의 초기식을 검증하기 위한 별도의 식
            Console.WriteLine(ii);        // 위의 초기식에서의 값은 5임
        }
    }
}
