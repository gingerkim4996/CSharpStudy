﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20240307 김진재
// C# 프로그래밍 한빛아카데미 참고
// p175
// 중첩반복문2

namespace Chapter_03
{
    internal class cs3_07_Nested_Loop_Example2    // 중첩반복문2
    {
        static void Main7(string[] args)
        {
            // 1. 중첩반복문을 통한 역피라미드 만들기1
            for (int i = 0; i < 5; i++) // 초기식 i = 0, 조건식 i < 5; 종결식 i++
                                        // i는 5과 같아지기 직전인 0부터 5까지 루프되어 5가 곧 총 루프의 횟수가 됨
            {
                for (int j = 0; j < 5 - i; j++) // 문장,초기식 int j = 0; 조건식 j < 5 - i; 종결식 j++ 
                    Console.Write(' ');
                for (int j = 0; j < i + 1 ; j++) // 문장,초기식 int j = 0; 조건식 j < 5 + 1; 종결식 j++ 
                    Console.Write('*');
                Console.Write('\n');
            }


            // 1. 중첩반복문을 통한 역피라미드 만들기2
            // 행의 변수 i, 열의 변수 j
            // # # # # *
            // # # # *
            // # # *
            // # *
            // *

            // int i, j;

            // 외부루프 초기식은 스타트 지점과 같음
            // 외부루프 조건식은 i를 몇 줄까지 출력할지
            // 외부루프 종결식은 그 줄까지 증가시키기 위해
            // 내부루프 초기식은 개수 스타트 지점
            // 내부루프 조건식은 j로 몇개의 별을 찍을지 혹은 내부루프를 두가지 두어 몇개의 공백을 둘지 정함
            for (int i = 1; i <= 5; i++)
            {
                for (int j = 1; j <= 5 - i; j++)
                {
                    Console.Write("*");
                }
                Console.Write("#\n");
            }

        }
    }
}
