﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20240304 김진재
// C# 프로그래밍 한빛아카데미 참고
// p162
// While 반복문은 가장 기본적인 반복문이다.
// if 조건문과 형식이 비슷하지만 if 조건문과 달리
// bool 표현식이 참인 동안 중활호 안의 문장을 계속 실행한다.

// while ( x < y ) <<<<< 이런 형태의 true or false의 bool 표현식이 괄호 안에 들어감
// {
//     결과 값이 만족할 때까지 반복
// }

// 조건이 변하지 않으면 무한히 반복되므로 반드시 조건을
// 거짓으로 만들수 있는 문장이 포함되어야 한다. 
// 위의 식처럼 진행할 경우 무한루프 하게 된다.


namespace Chapter_03
{
    internal class cs3_02_While
    {
        static void Main2(string[] args)
        {
            int i = 0;                          // int자료형의 i를 0으로 선언
            int[] intArray = { 110, 111, 303, 717, 305 }; // int자료형의 배열 intArray를 선언 후 1 2 3 4 5를 대입

            Console.WriteLine("intArray의 범위는 " + intArray.Length);
            while (i < intArray.Length)         // i가 intArray의 범위(5) 보다 작을 경우 반복하는 반복문
            {
                Console.WriteLine(i + "번째 출력: " + intArray[i]); // i(첫 반복시 0)와 intArray의 범위(5)가 될때까지 
                i++;                                                // 1을 추가하게 되고 intArray의 최대범위가 되면 반복문이 종료됨.
            }
        }
    }
}
