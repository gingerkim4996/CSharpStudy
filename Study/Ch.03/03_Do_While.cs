﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20240304 김진재
// C# 프로그래밍 한빛아카데미 참고
// p164
// While 반복문은 조건을 먼저 검사하고 코드를 반복적으로 실행.
// 이 While문은 조건(오른쪽 괄호에 있는 bool 값으로 판단하는)을 틀리면
// 한번도 작동하지 않는 문제점이 있다.
// 때문에 Do While은 조건과 관계 없이 최소한 한번 실행해야하는 경우 사용한다.

// while과 do while은 형태가 비슷하지만 조건(While과 옆에 있는 소괄호 조건식)을
// 비교하는 부분이 마지막에 위치한다.

// do
// {
//     bool값이 거짓일 경우 실행할 문장
// } while (bool 조건)

namespace Chapter_03
{
    internal class cs3_03_Do_While     // 문장이 한번은 꼭 실행되어야 하는 특수한 목적에서만 사용됨
    {
        static void Main3(string[] args)
        {
            // 사용자에게 입력을 지속적으로 받고 입력값이 exit일때 종료하게 되는 코드
            string input; 
            do
            {
                Console.Write("입력(exit을 입력하면 종료) : "); // 이 문장이 계속 나옴
                input = Console.ReadLine();                   // ReadLine에 Input이 입력됨
            } while (input != "exit");                       // exit이면 코드가 종료됨.
                                                             // 아닐 경우 do로 다시 돌아가 반복.
        }
    }


}
