﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

// 20240409 김진재
// 침대코딩 BedCoding
// [C# 코딩 강의10] this와 this를 통한 생성자 호출
// https://youtu.be/bJuMMy4NLHg?si

// this의 활용법
// 생성자가 상위 클래스에서 선언된 변수를 가져올때 사용할 수 있다.
// 아래와 같이 상위 클래스에서 선언된 name2 = "1"의 경우
// this.name2로 name2에 초기화된 1을 가져올 수 있게 된다.

namespace Chapter_05
{
    internal class cs5_01_this
    {   
        // 1. 생성자(Constructor)를 이용한 this 사용법
        public class Const // 클래스의 이름은 생성자의 Const와 같다.
        {
            private string name1 = "이것은 ";
            public Const() // 생성자의 이름은 클래스의 이름인 Const와 같아야한다.
            {
                  Console.WriteLine(this.name1);// 클래스의 이름을 알면 생성자도 쉽게 알 수 있기 때문
            }                                   // 이러한 규칙은 코드를 읽고 이해하기 쉽게 만든다.
        }

        static void Main(string[] args)
        {
            Const cs = new Const(); // Const를 객체화 하여 호출하였다.
        }
    }
}
