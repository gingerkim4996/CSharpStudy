﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20231217 김진재
// C# 프로그래밍 한빛아카데미 참고
// p46 ~ p75

namespace Chapter_01
{
    internal class cs1_01_WriteLine // 클래스명은 숫자로 시작할 수 없다.
    {
        static void Main1(string[] args)
        {
            // 1.WriteLine과 Write
            Console.WriteLine("WriteLine"); // 개행.
            Console.WriteLine("HelloWorld");
            Console.Write("Write");         // 개행 안됨.
            Console.Write("HelloWorld");

            // 2.따옴표와 사칙연산
            Console.WriteLine("5 + 3 * 2"); // 쌍따옴표가 있으면 문자열로 인식함.
            Console.WriteLine(5 + 3 * 2);   // 쌍따옴표가 없으면 해당 숫자들을 계산후 출력.
            Console.WriteLine((5 + 3) * 2); // 사칙연산처럼 괄호로 먼저 계산하게 할 수 있음.

            // 3.정수와 실수
            Console.WriteLine(1);           // 정수.
            Console.WriteLine(1.1);         // 실수.

            // 4.정수와 실수의 연산
            Console.WriteLine(7 / 3);       // 정수끼리 계산시 소수점은 버림.
            Console.WriteLine(7.5 / 3.1);   // 정수가 아닌 실수는 소수점을 출력함.
            Console.WriteLine(-4 * -7);

            // 5.문자
            Console.WriteLine('A');         // 글자 하나를 타나내는 자료형.
            Console.WriteLine('가');        // 작은 따옴표로 나타냄.
            Console.WriteLine('1');

            // 6.문자열
            Console.WriteLine("abcdefg");   // 문자의 집합을 나타내는 자료형.
            Console.WriteLine("가나다라");   // 큰 따옴표로 나타냄.

            // 7.문자열 연결
            Console.WriteLine("가나다" + "나다라" + "마바사"); // 문자열은 가능하지만
            Console.WriteLine('가' + '나' + '다'); // 작은 따옴표의 문자는 더하면 숫자로 출력됨.

            // 8.문자열 중 문자 선택
            Console.WriteLine("가나다라"[0]); // 문자열의 가장 첫번째 칸은 0으로 시작함.
                                             // [0]의 숫자를 바꿔 선택 가능.
                                             // 다만 문자열의 갯수를 벗어난 수는 오류가 발생.

            // 9.이스케이프 문자
            Console.WriteLine("가\t나다라"); // 수평 띄어쓰기
            Console.WriteLine("가\\나다라"); // 역 슬래시 출력
            Console.WriteLine("가\n나다라"); // 줄 바꿈
            Console.WriteLine("가\"나다라"); // 큰 따옴표 출력

            // 10.Bool로 보는 참과 거짓
            Console.WriteLine("True");      // 단순히 문자열로 출력됨
            Console.WriteLine(52 < 273);    // 값이 정답이기 때문 True로 출력
            Console.WriteLine(52 > 273);    // 값이 오답이기 때문 False로 출력
            
            // 11.논리 부정 연산자 (!)
            Console.WriteLine(!true);       // !는 값을 반대로 출력 true를 부정함으로 false가 출력됨
            Console.WriteLine(!false);      // !는 값을 반대로 출력 false를 부정함으로 true가 출력됨
            Console.WriteLine(52 < 273);    // 값이 정답이기 때문 True로 출력
            Console.WriteLine(!(52 < 273)); // 값이 정답이기에 True가 나오지만 반대로 출력해 false
            Console.WriteLine(!!(52 < 273)); // 값이 정답이기에 True가 나오지만 반대로 출력해 false한 걸
                                             // 반대로 출력해 True

            // 12.논리 연산자와 bool
            int a = 10, b = 20;
            Console.WriteLine("a < b = {0}", a < b);   // a는 b보다 작다 O true
            Console.WriteLine("a > b = {0}", a > b);   // a는 b보다 크다 X false
            Console.WriteLine("a <= b = {0}", a <= b); // a는 b보다 크거나 같다 O true
            Console.WriteLine("a >= b = {0}", a >= b); // a는 b보다 작거나 같다 X true
            Console.WriteLine("a == b = {0}", a == b); // a는 b와 같다 X false
            Console.WriteLine("a != b = {0}", a != b); // a는 b와 다르다 O true
        }
    }
}
