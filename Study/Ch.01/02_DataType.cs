﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20231218 김진재
// C# 프로그래밍 한빛아카데미 참고
// p79
// 20240227 김진재
// 케이디 유니티로 배우는 C# 강좌 자료형

namespace Chapter_01
{
    internal class cs1_02_DataType // 자료형
    {
        static void Main(string[] args)
        {
            // 1.자료형 사용 예시
            double pi = 3.141592; // 자료형과 변수를 선언하다.
            Console.WriteLine(pi);

            // 2.정수 자료형
            int a = 10;
            long b = 20;
            Console.WriteLine("int는 정수이며 자료형의 크기는 " + sizeof(int) +
                              " 최대 값은 " + int.MaxValue +
                              " 현재 대입된 값은 " + a + " 입니다.");
            Console.WriteLine("long는 정수이며 자료형의 크기는 " + sizeof(long) +
                              " 최대 값은 " + long.MaxValue +
                              " 현재 대입된 값은 " + b + " 입니다.\n");

            // 3.실수 자료형
            float c = 30;
            double d = 40;
            Console.WriteLine("float는 실수이며 자료형의 크기는 " + sizeof(float) +
                              " 최대 값은 " + float.MaxValue +
                              " 현재 대입된 값은 "+ c + " 입니다.");
            Console.WriteLine("double는 정수이며 자료형의 크기는 " + sizeof(double) +
                              " 최대 값은 " + double.MaxValue +
                              " 현재 대입된 값은 " + d + " 입니다.\n");

            // 4.문자 자료형
            char A = 'A';
            Console.WriteLine("char는 문자이며 자료형의 크기는 " + sizeof(char) +
                              " 최대 값은 " + char.MaxValue +
                              " 현재 대입된 값은 "+ A + " 입니다.\n");
            
            // 5.문자열 자료형
            string Message = "안녕하세요";
            Console.WriteLine("string는 문자열이며 현재 대입된 값은 " + Message +
                              "!" + " 입니다.");
            Console.WriteLine("string의 1번째 문자는 \"" + Message[0] +
                              "\" 이며 3번째 문자는 \"" + Message[2] +
                              "\" 이며 5번째 문자는 \"" + Message[4] + "\" 입니다.\n");

            // 6.Bool 자료형
            bool one = 10 < 0;
            bool other = 20 > 100;

            Console.WriteLine("bool은 boolean 형이라고 합니다. boolean 형은 참과 거짓을 가리는 자료형 입니다.");
            Console.WriteLine("one에 할당된 식은 10 < 0이며 other에 할당된 식은 20 > 100 입니다.\n" +
                              "10은 0보다 크기에 이 식은 X으로 one의 값은 " + one + " 입니다.\n" +
                              "20은 100보다 작기에 이 식은 X으로 other의 값은 " + other + " 입니다.");

            
            // 7. 서로 다른 자료형 간의 연산
            // 7.1. long과 int 연산하기
            int a2 = 500;
            long b2 = 700;
            long sum = a2 + b2; // 값이 큰 long과 값이 작은 int라도
                                // 숫자를 담는 범위가 더 넓은 long으로 적용시 연산이 가능하다.

            long a3 = 600;
            int b3 = 700;
            long sum2 = a3 + b3; // 마찬가지로 값이 작은 long과 큰 int라도
                                 // 숫자를 담는 범위가 넓은 long이라 연산이 가능하다.
            
            // 7.2. long과 int를 변환하여 연산하기
            long a4 = 800;
            int b4 = 700;
            int sum3 = (int)a4 + b4; // long(a4) 보다 작은 int(b4)와 연산하기 위해서는 범위인 int로 변환하여야 한다.
                                     // int sum3 = a4 + b4;
                                     // 심각도	코드	설명	프로젝트	파일	줄	비표시 오류(Suppression) 상태	세부 정보
                                     // 오류 CS0266  암시적으로 'long' 형식을 'int' 형식으로 변환할 수 없습니다.
                                     // 명시적 변환이 있습니다.캐스트가 있는지 확인하세요.
                                     // 숫자를 담는 범위가 좁은 int에 long을 연산시 오류가 뜨며 실패한다.

            // 7.3. int와 float를 변환하여 연산하기
            int a5 = 100;
            float b5 = 100.15f;
            int sum4 = a5 + (int)b5; // 이 경우는 float가 실수이기에 int로 변환되어 소숫점 이하를 버리게 된다.
            int sum5 = (int)(a5 + b5); // 이 경우는 해당 연산 자체를 int로 변환하게 하여 소숫점 이하를 버리게 된다.
            float sum6 = a5 + b5;    // 이 경우는 flaot로 연산을 맞추어 변환하게 되어 소숫점 이하까지 포함하게 된다.

            // 7.4. float를 string으로 출력하기
            float a6 = 100;           // float로 선언되었음.
            string b6 = a6.ToString(); // 이를 문자열로 변환함. ToString은 메서드이지만 객체(a6)를 문자열로 변환하는게 전부이기에
                                       // 매개변수를 받을 일이 없어 빈 괄호로 유지함.
                                       // 이러면 a6의 100이 b6의 string으로 '문자열'이 되어 출력됨.

            // 7.5. string을 int로 출력하기
            int a7;                   // int로 선언되었음.
            string b7 = "10000"; 
            a7 = int.Parse(b7);       // int.Parse로 b7을 변환하여 a7에 대입하면 string인 b7이 int로 출력됨


            Console.WriteLine(sum);
            Console.WriteLine(sum2);
            Console.WriteLine(sum3);
            Console.WriteLine(sum4);
            Console.WriteLine(sum5);
            Console.WriteLine(sum6);
            Console.WriteLine(b6);
            Console.WriteLine(a7);
        }
    }
}
