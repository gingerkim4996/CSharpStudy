﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20240228 김진재
// C# 프로그래밍 한빛아카데미 참고
// p127

namespace Chapter_02
{
    internal class cs2_01_if
    {
        static void Main1(string[] args)
        {
            Console.Write("if 활용 \n숫자 입력: ");
            int input = int.Parse(Console.ReadLine()); // 자료형이 int인 input을 선언과 동시에 readline으로 받는 문자를 int로 변환.

            if(input % 2 == 0)                        // int로 받아들인 input을 2로 나누었을때 0이 될 시 짝수.
            {
                Console.WriteLine("짝수입니다.");
            }
            if(input % 2 == 1)                        // input을 2로 나누었을때 1이 남을시 홀수.
            {
                Console.WriteLine("홀수입니다.");            
            }

            if (DateTime.Now.Hour < 12) // 현재 시간에 따라 달라진다. 12시 이전엔 오전, 이후엔 오후
            {
                Console.WriteLine("현재 시각은 오전입니다.");
            }
            if (12 <= DateTime.Now.Hour)
            {
                Console.WriteLine("현재 시각은 오후입니다");
            }
        }
    }
}
