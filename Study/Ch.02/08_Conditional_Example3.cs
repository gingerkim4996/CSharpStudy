﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20230228 김진재
// C# 프로그래밍 한빛아카데미 참고
// p154

namespace Chapter_02
{
    internal class cs_08_Conditional_Example3 // 조건문 예제 
    {
        static void Main8(string[] args)
        {
            Console.Write("학년을 입력하세요 : ");
            int level = int.Parse(Console.ReadLine());

            switch (level)
            {
                case 1:
                    Console.WriteLine("1학년의 수강 전공 학점 : 12학점");
                    break;

                case 2:
                    Console.WriteLine("2학년의 수강 전공 학점 : 18학점");
                    break;

                case 3:
                    Console.WriteLine("3학년의 수강 전공 학점 : 10학점");
                    break;

                case 4:
                    Console.WriteLine("4학년의 수강 전공 학점 : 18학점");
                    break;
            }
        }
    }
}
