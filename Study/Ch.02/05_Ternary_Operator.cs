﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20230229 김진재
// C# 프로그래밍 한빛아카데미 참고
// p142

namespace Chapter_02
{
    internal class cs_05_Ternary_Operator // 삼항 연산자
    {
        static void Main5(string[] args)
        {
            Console.Write("숫자를 입력하세요 : ");
            string input = Console.ReadLine();
            int number = int.Parse(input);

            Console.WriteLine(number > 0 ? "자연수임." : "자연수가 아님.");
            // input에 의해 대입된 number가 해당 조건과 비교하여
            // 자연수인지 아닌지를 판별함.
        }
    }
}
