﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20230228 김진재
// C# 프로그래밍 한빛아카데미 참고
// p130

namespace Chapter_02
{
    internal class cs2_02_If_Else
    {
        static void Main2(string[] args)
        {
            Console.Write("if else 활용 \n숫자 입력 : ");
            int input = int.Parse(Console.ReadLine()); // 자료형이 int인 input을 선언과 동시에 readline으로 받는 문자를 int로 변환.

            if (input % 2 == 0)
            {
                Console.WriteLine("짝수입니다."); // int로 받아들인 input을 2로 나누었을때 0이 될 시 짝수.
            }

            else
            {
                Console.WriteLine("홀수입니다."); // 이외의 값은 홀수로 받음.
            }


            if (DateTime.Now.Hour < 11) // 현재 시간에 따라 달라진다. 11시 이전에는 아침, 이후 15시까지는 점심, 해당되지 않는 이후는 저녁
            {
                Console.WriteLine("아침 먹을 시간입니다.");
            }
            else
            {
                if (DateTime.Now.Hour < 15)
                {
                    Console.WriteLine("점심 먹을 시간입니다");
                }
                else
                {
                    Console.WriteLine("저녁 먹을 시간입니다.");
                }
            }
        }
    }
}
