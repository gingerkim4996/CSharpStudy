﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20230228 김진재
// C# 프로그래밍 한빛아카데미 참고
// p147

namespace Chapter_02
{
    internal class cs_07_Conditional_Example2 // 조건문 예제 
    {
        static void Main7(string[] args)
        {
            Console.WriteLine("방향키를 누르세요");
            ConsoleKeyInfo info = Console.ReadKey();
            switch(info.Key)
            {
                case ConsoleKey.UpArrow:
                    Console.WriteLine("위로 이동");
                    break;
                case ConsoleKey.RightArrow:
                    Console.WriteLine("오른쪽으로 이동");
                    break;
                case ConsoleKey.LeftArrow:
                    Console.WriteLine("왼쪽으로 이동");
                    break;
                case ConsoleKey.DownArrow:
                    Console.WriteLine("아래로 이동");
                    break;
                default:
                    Console.WriteLine("다른 키네요");
                    break;
            }
        }
    }
}
