﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20230228 김진재
// C# 프로그래밍 한빛아카데미 참고
// p144

namespace Chapter_02
{
    internal class cs_06_Conditional_Example // 조건문 예제 
    {
        static void Main6(string[] args)
        {
            Console.WriteLine("저희 인사할까요?");
            string line = Console.ReadLine();

            if(line.Contains("안녕"))
            {
                Console.WriteLine("안녕하세요!");
            }
            else
            {
                Console.WriteLine("ㅎㅇㅋㅋ");
            }
        }
    }
}
