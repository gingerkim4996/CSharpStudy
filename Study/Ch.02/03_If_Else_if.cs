﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// 20230228 김진재
// C# 프로그래밍 한빛아카데미 참고
// p134

namespace Chapter_02
{
    internal class cs_03_If_Else_if
    {
        static void Main3(string[] args)
        {
            Console.Write("학점을 입력하세요 : ");
            float score = float.Parse(Console.ReadLine()); // 자료형이 float인 score를 선언과 동시에 readline으로 받는 문자를 float로 변환.

            if (score == 4.5)
                Console.WriteLine("god");
            else if (4.2 <= score)
                Console.WriteLine("교수님 아들");
            else if (4.0 <= score)
                Console.WriteLine("공부 잘하네");
            else if (3.5 <= score)
                Console.WriteLine("소시민");
            else if (3.0 <= score)
                Console.WriteLine("크게 될 사람");
            else if (2.5 <= score)
                Console.WriteLine("국장 탈락");
            else if (2.0 <= score)
                Console.WriteLine("혁명의 씨앗");
            else
                Console.WriteLine("레전드");
        }
    }
}
